(defun f-to-c ( a )
  (+
   (* (/ 9 5) a)
   32.0
  ))

(defun my-abs (x)
   (if (> x 0) x (- x))
 )

(defun make-even (x)
	(if (oddp x) (+ x 1) x)
)

(defun order (a b)
   (if (< a b) (list a b) (list b a))
)

(defun compare (a b)
   (cond 	((eq a b) '(numbers are same))                      
   			((< a b) '(first is smaller))                     
   			((> a b) '(first is bigger))
   	)
)

(defun where-is (a)
    (cond ((eq a 'pokhara) 'Nepal)
          ((eq a 'Rome) 'Italy)
          ((eq a 'Barcelona) 'Spain)
          (t 'Unknown)
    )
)


(defun constrain (x min max) 
	(if (< x min ) min (if (> x max) max x ))
)

(defun firstzero (a)
    (cond ((eq (car a) 0) 'First)
          ((eq (cadr a) 0) 'Second)
          ((eq (caddr a) 0) 'Third)
          (t 'notPresent)
    )
 )

(defun  my-funct1 (a)
    (cond ((and (oddp a) (> a 0)) (* a a))
     	  ((and (oddp a) (< a 0)) (* 2 a))
          (t (/ a 2.0)))
)

(defun my-average (a b) 
    (let ((sum (+ a b))) (/ sum 2))
)

(defun price-change (old new)
    (let* ((diff (- old  new)) (prop (/ diff old)) (percent (* prop 100))) percent)
)

(defun small-positive-odd (b)
   (if (and  (< b 100) (> b 0) (oddp b)) t nil)
)